<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('test');
});
*/
Route::get('/', 'UsersController@index');
Route::get('/home', 'UsersController@index');
Route::get('/shop', 'UsersController@fnShop');
Route::get('/sales', 'UsersController@fnSales');
Route::get('/about', 'UsersController@fnAbout');
Route::get('/contact', 'UsersController@fnContact');
Route::get('/product-detail', 'UsersController@fnProductDetail');
Route::get('/contact', 'UsersController@fnContact');
Route::get('/cart', 'UsersController@fnCart');

//=================================================================================

Route::get('/admin', 'UsersController@fnGetAdminlogin');